#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_subwaysystem
----------------------------------

Tests for `subwaysystem` module.
"""

import unittest

from subwaysystem.subwaysystem import SubwaySystem


class TestSubwaySystem(unittest.TestCase):

    def setUp(self):
        self.ss = SubwaySystem()
        self.train_lines = {
            '1': ["Canal", "Houston", "Christopher", "14th"],
            '3': ["Chambers", "Park Place", 'Wall'],
            'E': ["Spring", "West 4th", "14th", "23rd"]
        }
        self.edges = {'1': [("Canal", "Houston", 3),
                            ("Houston", "Christopher", 7),
                            ("Christopher", "14th", 2)],
                      'E': [("Spring", "West 4th", 1),
                            ("West 4th", "14th", 5),
                            ("14th", "23rd", 2)]}

    def tearDown(self):
        self.ss = None
        self.train_lines = None


class TestAddTrainLines(TestSubwaySystem):

    def test_000_add_train_lines(self):
        # Test for addition of train lines
        assert self.ss.add_train_line(self.train_lines['1'], '1')
        assert self.ss.add_train_line(self.train_lines['E'], 'E')

    def test_001_add_train_lines(self):
        # Test addition of correct number of nodes
        self.ss.add_train_line(self.train_lines['1'], '1')
        assert self.ss.graph.number_of_nodes() == 4
        self.ss.add_train_line(self.train_lines['E'], 'E')
        assert self.ss.graph.number_of_nodes() == 7

    def test_002_add_with_time_between_stations(self):
        self.ss.add_train_line(self.train_lines['1'], '1',
                               time_between_stations=self.edges['1'])
        self.ss.add_train_line(self.train_lines['E'], 'E',
                               time_between_stations=self.edges['E'])
        assert self.ss.graph.number_of_nodes() == 7
        assert self.ss.graph.number_of_edges() == 6


class TestTakeTrain(TestSubwaySystem):
    def test_003_shortest_path(self):
        self.ss.add_train_line(self.train_lines['1'], '1')
        self.ss.add_train_line(self.train_lines['E'], 'E')
        self.ss.add_train_line(self.train_lines['3'], '3')
        print self.ss.graph.nodes()
        assert self.ss.take_train('Canal', 'Wall') == 'No path between these stops'
        assert self.ss.take_train('Houston', '23rd') == \
               ["Houston", "Christopher", "14th", "23rd"]

    def test_004_shortest_weighted_path(self):
        self.ss.add_train_line(self.train_lines['1'], '1',
                               time_between_stations=self.edges['1'])
        self.ss.add_train_line(self.train_lines['E'], 'E',
                               time_between_stations=self.edges['E'])
        trip = self.ss.take_train(origin="Houston",
                                  destination="23rd")
        assert trip == (["Houston", "Christopher", "14th", "23rd"], 11)

if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
