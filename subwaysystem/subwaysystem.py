# ! /usr/bin/env python
# -*- coding: utf-8 -*-
import networkx as nx


class SubwaySystem(object):
    """A simple subway model.

    This class provides a simple interface for modeling
    subway systems.

    Attributes:
        graph(networkx.Graph):  a networkx.Graph object for storing
                                nodes and edges.
        weighted(bool):         equals True if edges are weighted
                                else False.

    """
    def __init__(self):

        self.graph = nx.Graph()
        self.weighted = False

    def add_train_line(self, stops,
                       name, time_between_stations=None):
        """Add a train line to subway system.

        This method allows a user to add a train line to the subway system,
        with or without times between stations.

        :param stops:                   list of string objects
                                        identifying names of subway stops.
        :param name:                    name of subway line.
        :param time_between_stations:   list of tuples representing edges
                                        (origin node, destination node,
                                        time between nodes)
        :return: True if successful, otherwise False.
        """

        try:
            # add stations to Graph object as nodes.
            self.graph.add_nodes_from(stops, line=name)
            if time_between_stations is None:
                # build list of unweighted edges.
                edges = [(stops[i-1], stops[i])
                         for i in range(1, len(stops))]
                # add edges to Graph object
                self.graph.add_edges_from(edges, line=name)
            else:
                # Add weighted edges to Graph and make note
                # in self.weighted for ease of reference.
                self.weighted = True
                self.graph.add_weighted_edges_from(time_between_stations, line=name)
            return True
        except:
            return False

    def take_train(self, origin, destination):
        """Identify fastest route on subway system

        This method identifies the route with either the fewest
        number of stops or, in cases where times between stations have
        been provided when adding train lines, the least amount of
        travel time

        :param origin:      the starting point of the trip.
        :param destination: the end point of the trip.
        :return:            either a list of station names if no times
                            between stations have been provided, or a
                            tuple comprised of a list of stations and the
                            total travel time.
        """
        try:
            # get shortest path
            path = self.get_path(self.graph, origin,
                                 destination, self.weighted)

            # get length
            length = self.get_length(self.graph, origin,
                                     destination, self.weighted)

            if length is not None:
                # return ([stops], length_of_trip)
                return path, length
            # return [stops]
            return path

        except nx.NetworkXNoPath:
            return 'Warning: no path between these stops. Is ' \
                   'there construction going on again?'

    @staticmethod
    def get_path(g, o, d, weighted):
        # if times between stations have been provided
        # get shortest path based on travel time.
        if weighted:
            return nx.dijkstra_path(g, o, d)
        # otherwise, get shortest path based on number
        # of stops.
        return nx.shortest_path(g, o, d)

    @staticmethod
    def get_length(g, o, d, weighted):
        # if times between stations have been provided
        # get travel time. Otherwise return None.
        if weighted:
            return nx.dijkstra_path_length(g, o, d)
        return None
