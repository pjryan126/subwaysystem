# SubwaySystem
A Python package for modeling a city's subway system.

# Installation

```
$ mkdir SubwaySystem
$ cd SubwaySystem
$ virtualenv --no-site-packages venv
$ source venv/bin/activate
$ git clone https://bitbucket.org/pjryan126/subwaysystem.git
$ cd subwaysystem
$ python setup.py install
```

# Usage


```
$ python
>>> from subwaysystem.subwaysystem import SubwaySystem
>>> subway_system = SubwaySystem()
```

## Example (Challenge 1)
```
>>> subway_system.add_train_line(stops=["Canal", "Houston", "Christopher", "14th"], name="1")
True
>>> subway_system.add_train_line(stops=["Spring", "West 4th", "14th", "23rd"], name="E")
True
>>> subway_system.take_train(origin="Houston", destination="23rd")
["Houston", "Christopher", "14th", "23rd"]
```

## Example (Challenge 2)
```
>>> subway_system.add_train_line(stops=["Canal", "Houston", "Christopher", "14th"], name="1",
...     time_between_stations=[("Canal", "Houston", 3),
...                            ("Houston", "Christopher", 7),
...                            ("Christopher", "14th", 2),
...                            ])
True
>>> subway_system.add_train_line(stops=["Spring", "West 4th", "14th", "23rd"], name="E"
...     time_between_stations=[("Spring", "West 4th", 1),
...                             ("West 4th", "14th", 5),
...                             ("14th", "23rd", 2),
...                             ])
True
>>> subway_system.take_train(origin="Houston", destination="23rd")
(["Houston", "Christopher", "14th", "23rd"], 11)
```

