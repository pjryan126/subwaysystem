============
Installation
============

At the command line::

    $ easy_install subwaysystem

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv subwaysystem
    $ pip install subwaysystem
