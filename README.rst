===============================
SubwaySystem
===============================

.. image:: https://img.shields.io/pypi/v/subwaysystem.svg
        :target: https://pypi.python.org/pypi/subwaysystem

.. image:: https://img.shields.io/travis/pjryan126/subwaysystem.svg
        :target: https://travis-ci.org/pjryan126/subwaysystem

.. image:: https://readthedocs.org/projects/subwaysystem/badge/?version=latest
        :target: https://readthedocs.org/projects/subwaysystem/?badge=latest
        :alt: Documentation Status


Coding exercise for model of a subway system

* Free software: ISC license
* Documentation: https://subwaysystem.readthedocs.org.

Features
--------

* TODO

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-pypackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
